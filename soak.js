export let options = {
    stages: [
        { duration: '2m', target: 200 }, // ramp up to 200 users
        { duration: '0h56m', target: 200 }, // stay at 200 for ~1 hour
        { duration: '2m', target: 0 }, // scale down. (optional)
    ]
};
