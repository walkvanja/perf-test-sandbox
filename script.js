import http from 'k6/http';
import { check, sleep } from 'k6';

// Setup
const API_URL = "https://petstore.swagger.io/v2/user";
const API_HEADER = { "Content-Type": "application/json", "accept": "application/json" };
const randomWord = () => (Math.random() + 1).toString(36).substring(7);

export default function () {
    // Request
    const params = { headers: API_HEADER };

    // Fake data
    let fname = randomWord();
    let lname = randomWord();

    // User Object
    let user = {
        "username": `${fname}`,
        "firstName": `${fname}`,
        "lastName": `${lname}`,
        "email": `${fname}.${lname}@server.com`,
        "password": `${lname}${lname}`,
        "phone": "+123456789",
        "userStatus": 0
    };
    let userUpdate = {
        "phone": "+987654321",
        "userStatus": 1
    }

    // Create User request
    let createUserResponse = http.post(
        `${API_URL}`,
        JSON.stringify(user),
        params
    );
    check(
        createUserResponse,
        { "'Create User'\tresponse status code is 200": (r) => r.status === 200 }
    );
    // specific of the petstore.swagger.io API
    let createdUserId = fname;

    // Read User request
    let getUserResponse = http.get(
        `${API_URL}/${createdUserId}`,
        params
    )
    check(
        getUserResponse,
        { "'Read User'\tresponse status code is 200": (r) => r.status === 200 }
    );

    // Update User request
    let patchUserResponse = http.put(
        `${API_URL}/${createdUserId}`,
        JSON.stringify(userUpdate),
        params
    )
    check(
        patchUserResponse,
        { "'Update User'\tresponse status code is 200": (r) => r.status === 200 }
    );

    // Delete User request
    let deleteUserResponse = http.del(
        `${API_URL}/${createdUserId}`,
        params
    )
    check(
        deleteUserResponse,
        { "'Delete User'\tresponse status code is 200": (r) => r.status === 200 }
    );

    // Pause 1 sec
    sleep(1);
}
