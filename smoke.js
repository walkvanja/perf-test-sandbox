export const options = {
    // 1 user looping for 1 minute
    vus: 1,
    duration: '1m',

    // 99% of requests must complete below 0.25s
    thresholds: {
        http_req_duration: ['p(99)<250'],
    },
};
