# Perf Test Sandbox

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Goal](#goal)
- [Running the Tests](#running-the-tests)
  - [Requirements](#requirements)
  - [Information](#information)
  - [Used Docs](#used-docs)
  - [Run](#run)
    - [Smoke Test](#smoke-test)
    - [Load Test](#load-test)
    - [Stress Testing](#stress-testing)
    - [Soak Testing](#soak-testing)
- [Next Steps](#next-steps)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Goal

Complete [homework](https://gist.github.com/YegorMaksymchuk/fe6b1aa04d66d71fdbc394c7e206fce4#file-perf-test-jmeter-plan-of-lesson) using [k6 modern load testing tool](https://k6.io/).

## Running the Tests

### Requirements

1. [Docker](https://docs.docker.com/get-docker/)
2. 14Gb RAM for stress and soak tests, 2+ CPU cores
3. PowerShell
4. Windows OS (all the commands are adopted for windows)

### Information

To run the test 2 scripts are used:

- The main script to test `petstore.swagger.io` `User` CRUD is [script.js](script.js). 
- Options script to configure respective testing scenario ([smoke.js](smoke.js), [load.js](load.js), [stress.js](stress.js), [soak.js](soak.js)).

### Used Docs

- https://k6.io/docs/get-started/running-k6/
- https://k6.io/docs/get-started/results-output/
- https://k6.io/docs/using-k6/k6-options/
- https://k6.io/docs/using-k6/thresholds/
- https://k6.io/docs/test-types/introduction/

### Run

#### Smoke Test

Quick minimal load test (details - [smoke.js](smoke.js)).

```
Get-Content .\script.js,.\smoke.js | docker run --rm -i grafana/k6 run -
```

Sample report:
![](https://img.newtonideas.com/pUqY1e4cSNvhaZDCzkV0.png)

Means smoke test successfully passed

#### Load Test

Verify system is meeting the performance goals (details - [load.js](load.js))

```
Get-Content .\script.js,.\load.js | docker run --rm -i grafana/k6 run -
```

Sample report:
![](https://img.newtonideas.com/KTvNM1GWUfqRCKheCkds.png)

Means system is OK, however advised to run more tests to verify the system.

#### Stress Testing

Verify the availability and stability of the system under heavy load (details - [stress.js](stress.js)).

```
Get-Content .\script.js,.\stress.js | docker run --rm -i grafana/k6 run -
```

Sample report:
![](https://img.newtonideas.com/V8PM3lK9QYyYhHcF9t7K.png)

Means system require inspection and RCA of the failure in `GET` and `DELETE` REST API.

#### Soak Testing

Verify the reliability over a longer period of time (details - [soak.js](soak.js)).

```
Get-Content .\script.js,.\soak.js | docker run --rm -i grafana/k6 run -
```

Sample report:
![](https://img.newtonideas.com/CvHB0RBZoCyApbVCzkEU.png)

Means system require inspection and RCA of the failure in `GET` and `DELETE` REST API.

## Next Steps

1. Use Grafana [to visualize results](https://medium.com/swlh/beautiful-load-testing-with-k6-and-docker-compose-4454edb3a2e3)
